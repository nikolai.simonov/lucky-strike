import 'dart:ui';

import 'package:flutter/material.dart';

const RED = const Color(0xfff8191e);
const GOLD = const Color(0xffc5be88);

class AppTheme {
  static ThemeData themeData = new ThemeData(
    fontFamily: 'IBM Plex Sans',
    primaryColor: RED,
    accentColor: GOLD,
  );
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:path_provider/path_provider.dart';

class LuckyGuys {
  static LuckyGuys _instance;

  Random _random;

  factory LuckyGuys() {
    if (_instance == null) {
      _instance = new LuckyGuys._internal(new Random());
      _instance.init();
    }
    return _instance;
  }

  LuckyGuys._internal(this._random);

  List<String> _list = <String>[];

  List<String> getList() {
    return _list;
  }

  String pickLucky() {
    final luckyIndex = _random.nextInt(_list.length);
    final guy = _list[luckyIndex];
    return guy;
  }

  Future<List<String>> readFromCache() async {
    try {
      final file = await _cacheFile;

      String contents = await file.readAsString();

      final list = json.decode(contents).map<String>((f) {
        return '$f';
      }).toList();

      return list;
    } catch (e) {
      return [];
    }
  }

  Future<File> writeToCache() async {
    final file = await _cacheFile;
    final serializedList = json.encode(_list);
    return file.writeAsString(serializedList);
  }

  Future<File> get _cacheFile async {
    final directory = await getApplicationDocumentsDirectory();
    return new File('${directory.path}/cache.json');
  }

  void init() async {
    _list = await _instance.readFromCache();
  }
}

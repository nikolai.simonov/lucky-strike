import 'package:flutter/material.dart';
import 'package:lucky_strike/app_localizations.dart';
import 'package:lucky_strike/consts.dart';
import 'package:lucky_strike/pages/home_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class LuckyLottery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English
        const Locale('ru', 'RU'), // Russian
      ],
      onGenerateTitle: (BuildContext context) =>
          AppLocalizations.of(context).title,
      theme: AppTheme.themeData,
      home: new HomePage(),
    );
  }
}

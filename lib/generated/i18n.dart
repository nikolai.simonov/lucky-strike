import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class S extends WidgetsLocalizations {
  Locale _locale;
  String _lang;

  S(this._locale) {
    _lang = getLang(_locale);
    print('Current locale: $_lang');
  }

  static final GeneratedLocalizationsDelegate delegate =
      new GeneratedLocalizationsDelegate();

  static S of(BuildContext context) {
    var s = Localizations.of<S>(context, WidgetsLocalizations);
    s._lang = getLang(s._locale);
    return s;
  }

  @override
  TextDirection get textDirection => TextDirection.ltr;

  String get cancel_button => "Cancel";
  String get add_dialog_text_field_label => "The name of the new Dude[s]";
  String get add_button => "Add";
  String get add_dialog_desc =>
      "If you want to add more than one Dude you can separate the names with comma.";
  String get winner_screen_congrats => "congrats!";
  String get chose_one_tab => "CHOOSE ONE";
  String get add_to_list_title => "Add to list";
  String get title => "Chosen One";
  String get winner_screen_titles => "Winner";
  String get add_dialog_text_field_hint => "eg. James, Wally, etc...";
  String get composer_tab => "COMPOSE LIST";
  String get pick_chosen_one_btn => "Pick The Next Chosen One";
  String get winner_screen_winner_is => "Winer is";
}

class ru extends S {
  ru(Locale locale) : super(locale);

  @override
  TextDirection get textDirection => TextDirection.ltr;
}

class en extends S {
  en(Locale locale) : super(locale);
}

class GeneratedLocalizationsDelegate
    extends LocalizationsDelegate<WidgetsLocalizations> {
  const GeneratedLocalizationsDelegate();

  List<Locale> get supportedLocales {
    return [
      new Locale("ru", ""),
      new Locale("en", ""),
    ];
  }

  LocaleResolutionCallback resolution({Locale fallback}) {
    return (Locale locale, Iterable<Locale> supported) {
      var languageLocale = new Locale(locale.languageCode, "");
      if (supported.contains(locale))
        return locale;
      else if (supported.contains(languageLocale))
        return languageLocale;
      else {
        var fallbackLocale = fallback ?? supported.first;
        return fallbackLocale;
      }
    };
  }

  Future<WidgetsLocalizations> load(Locale locale) {
    String lang = getLang(locale);
    switch (lang) {
      case "ru":
        return new SynchronousFuture<WidgetsLocalizations>(new ru(locale));
      case "en":
        return new SynchronousFuture<WidgetsLocalizations>(new en(locale));
      default:
        return new SynchronousFuture<WidgetsLocalizations>(new S(locale));
    }
  }

  bool isSupported(Locale locale) => supportedLocales.contains(locale);

  bool shouldReload(GeneratedLocalizationsDelegate old) => false;
}

String getLang(Locale l) => l.countryCode != null && l.countryCode.isEmpty
    ? l.languageCode
    : l.toString();

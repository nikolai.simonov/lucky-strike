import 'package:flutter/material.dart';
import 'package:lucky_strike/app_localizations.dart';
import 'package:lucky_strike/consts.dart';
import 'package:lucky_strike/data/lucky_guys.dart';

import 'package:lucky_strike/pages/tabs/choser.dart';
import 'package:lucky_strike/pages/tabs/composer.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => new _MainPageState();
}

class _MainPageState extends State<HomePage> with TickerProviderStateMixin {
  LuckyGuys _luckiestGuys;
  int _initIndex;
  TabController _tabController;

  AnimationController _animationController;

  Animation<double> _fabOpacity;

  int _page;

  @override
  void initState() {
    super.initState();

    _luckiestGuys = new LuckyGuys();

    _initIndex = _luckiestGuys.getList().length > 0 ? 0 : 1;

    _tabController =
        new TabController(length: 2, initialIndex: _initIndex, vsync: this);

    _animationController = new AnimationController(
        duration: const Duration(milliseconds: 250), vsync: this);

    _fabOpacity = new Tween(begin: 0.0, end: 1.0).animate(
      new CurvedAnimation(
        parent: _animationController,
        curve: Curves.ease,
      ),
    )..addListener(() {
        setState(() {});
      });

    _tabController.addListener(() => setState(() {
          _page = _tabController.index;
          _animationController.animateTo(_page == 0 ? 0.0 : 1.0);
        }));

    _page = _initIndex;
    _animationController.animateTo(_initIndex == 0 ? 0.0 : 1.0);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        actions: <Widget>[
          new Container(
            margin: const EdgeInsets.all(8.0),
            child: _page == 0
                ? null
                : new IconButton(
                    icon: new Icon(Icons.save),
                    tooltip: 'Save list',
                    onPressed: () => _luckiestGuys.writeToCache(),
                  ),
          )
        ],
        title: new Text(AppLocalizations.of(context).title),
        bottom: new TabBar(
            controller: _tabController,
            indicatorColor: Colors.black,
            labelStyle: new TextStyle(
                color: Colors.black,
                fontFamily: 'IBM Plex Sans Condensed',
                fontWeight: FontWeight.bold),
            tabs: [
              new Tab(text: AppLocalizations.of(context).choseOneTab),
              new Tab(text: AppLocalizations.of(context).composerTab)
            ]),
      ),
      body: new TabBarView(
        controller: _tabController,
        children: [
          new ChoserTab(),
          new ComposerTab(),
        ],
      ),
      floatingActionButton: new Opacity(
        opacity: _fabOpacity.value,
        child: new FloatingActionButton(
          onPressed: _page == 0 ? null : _addDude,
          tooltip: 'Add a new dude',
          child: new Icon(Icons.add),
        ),
      ),
    );
  }

  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  final TextEditingController _controller = new TextEditingController();

  _addDude() {
    var dialog = new SimpleDialog(
      contentPadding: const EdgeInsets.only(right: 12.0, left: 12.0),
      titlePadding: const EdgeInsets.only(left: 12.0),
      title: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Text(AppLocalizations.of(context).addToListTitle),
            new IconButton(
              icon: new Icon(Icons.close),
              onPressed: () => Navigator.pop(context),
            )
          ]),
      children: <Widget>[
        new Text(AppLocalizations.of(context).addDialogDesc,
            style: new TextStyle(color: Colors.grey)),
        new TextField(
          autofocus: true,
          controller: _controller,
          decoration: new InputDecoration(
              labelText: AppLocalizations.of(context).addDialogTextFieldLabel,
              hintText: AppLocalizations.of(context).addDialogTextFieldHint),
        ),
        new Padding(
          padding: const EdgeInsets.only(top: 24.0, bottom: 12.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new FlatButton(
                  child: new Text(AppLocalizations.of(context).cancelButton),
                  onPressed: () {
                    _controller.clear();
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  textColor: RED,
                  child: new Text(AppLocalizations.of(context).addButton),
                  onPressed: () {
                    setState(() {
                      var list = _controller.text
                          .split(',')
                          .map((f) => f.trim())
                          .where((f) => !_luckiestGuys.getList().contains(f))
                          .toList();

                      if (list.isNotEmpty) {
                        _luckiestGuys.getList().addAll(list);
                        //cache after add
                        _luckiestGuys.writeToCache();
                      }
                      _controller.clear();
                    });
                    Navigator.pop(context);
                  })
            ],
          ),
        )
      ],
    );

    showDialog<String>(context: context, builder: (BuildContext) => dialog);
  }
}

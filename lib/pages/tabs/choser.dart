import 'package:flutter/material.dart';
import 'package:lucky_strike/app_localizations.dart';
import 'package:lucky_strike/consts.dart';
import 'package:share/share.dart';
import 'package:lucky_strike/data/lucky_guys.dart';

class ChoserTab extends StatefulWidget {
  @override
  _ChoserTab createState() => new _ChoserTab();
}

class _ChoserTab extends State<ChoserTab> {
  LuckyGuys luckyGuys;

  @override
  initState() {
    super.initState();
    luckyGuys = new LuckyGuys();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(8.0),
      child: new Stack(
        children: <Widget>[
          new Align(
              alignment: Alignment.topCenter,
              child: new Wrap(
                spacing: 8.0, // gap between adjacent chips
                runSpacing: 8.0, // gap between lines
                children: luckyGuys.getList().map((f) {
                  return _buildChip(f);
                }).toList(),
              )),
          new Align(
            child: new OutlineButton(
              borderSide: new BorderSide(
                  color: RED, width: 4.0, style: BorderStyle.solid),
              splashColor: RED,
              child: new Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Icon(Icons.chevron_right),
                      new Text(AppLocalizations.of(context).pickChosenOneBtn),
                      new Icon(Icons.chevron_left)
                    ],
                  )),
              onPressed: _getLucky,
            ),
            alignment: Alignment.bottomCenter,
          )
        ],
      ),
    );
  }

  Widget _buildChip(title) {
    return new Hero(
        tag: title,
        child: new Container(
          decoration: new BoxDecoration(
              color: Colors.white,
              border: new Border.all(
                color: Colors.black,
                width: 2.0,
              )),
          child: new Padding(
            padding: const EdgeInsets.all(6.0),
            child: new Text(
              title,
              style: new TextStyle(
                  fontFamily: 'IBM Plex Mono', fontWeight: FontWeight.normal),
            ),
          ),
        ));
  }

  void _getLucky() {
    setState(() {
      final luckyOne = luckyGuys.pickLucky();
      _showLuckyGuy(luckyOne);
    });
  }

  _showLuckyGuy(String who) {
    Navigator
        .of(context)
        .push(new MaterialPageRoute(
            builder: (context) => _buildWinnerScreen(context, who),
            fullscreenDialog: true))
        .then((some) => luckyGuys.getList().remove(who));
  }

  Widget _buildWinnerScreen(BuildContext context, String who) {
    return new Scaffold(
        key: PageStorageKey(who),
        appBar: new AppBar(
          title: new Text(AppLocalizations.of(context).winnerScreenTitles),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.share),
              onPressed: () {
                share("winner is $who");
              },
            )
          ],
        ),
        body: new Container(
            padding: const EdgeInsets.all(8.0),
            alignment: Alignment.center,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: new Text(
                        AppLocalizations.of(context).winnerScreenChosenOne)),
                new Hero(
                    tag: who,
                    child: new Container(
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          border: new Border.all(
                            color: RED,
                            width: 2.0,
                          )),
                      child: new Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: new Text(
                          who,
                          style: new TextStyle(
                              fontSize: 24.0,
                              fontFamily: 'IBM Plex Mono',
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                    )),
                new Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: new Text(
                        AppLocalizations.of(context).winnerScreenCongrats)),
              ],
            )));
  }
}

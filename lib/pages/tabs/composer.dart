import 'package:flutter/material.dart';
import 'package:lucky_strike/data/lucky_guys.dart';

class ComposerTab extends StatefulWidget {
  @override
  _ComposerTab createState() => new _ComposerTab();
}

class _ComposerTab extends State<ComposerTab> {
  LuckyGuys luckiestGuys;

  @override
  initState() {
    super.initState();
    luckiestGuys = new LuckyGuys();
  }

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemBuilder: (BuildContext context, int index) => new Dismissible(
          key: new PageStorageKey<String>(luckiestGuys.getList()[index]),
          direction: DismissDirection.horizontal,
          onDismissed: (DismissDirection dim) {
            setState(() {
              luckiestGuys.getList().removeAt(index);
            });
          },
          child: new Column(key: new ObjectKey(index), children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: new EdgeInsets.all(8.0),
              child: new Text('${index+1}. ${luckiestGuys.getList()[index]}'),
            ),
            new Divider()
          ])),
      itemCount: luckiestGuys.getList().length,
    );
  }
}

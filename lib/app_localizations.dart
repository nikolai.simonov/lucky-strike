import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

//todo consider moving to the Flutter's Intl tool 
class AppLocalizations {
  AppLocalizations(this.locale);

  final Locale locale;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      "title": "Chosen One",
      "chose_one_tab": "CHOOSE ONE",
      "composer_tab": "COMPOSE LIST",
      "pick_chosen_one_btn": "Pick The Next Chosen One",
      "add_to_list_title": "Add to list",
      "add_button": "Add",
      "cancel_button": "Cancel",
      "add_dialog_desc":
          "If you want to add more than one Dude you can separate the names with comma.",
      "add_dialog_text_field_label": "The name of the new Dude[s]",
      "add_dialog_text_field_hint": "eg. James, Wally, etc...",
      "winner_screen_titles": "Winner",
      "winner_screen_chosen_one": "Chosen one",
      "winner_screen_congrats": "congrats!"
    },
    'ru': {
      "title": "Избранный",
      "chose_one_tab": "ВЫБРАТЬ ЕГО",
      "composer_tab": "СОСТАВИТЬ СПИСОК",
      "pick_chosen_one_btn": "Следующий избранный",
      "add_to_list_title": "Добавить в список",
      "add_button": "Добавить",
      "cancel_button": "Отмена",
      "add_dialog_desc":
          "Если нужно добавить несколько чуваков, вы можете разделить их имена запятыми.",
      "add_dialog_text_field_label": "Имя нового чувак[а/ов]",
      "add_dialog_text_field_hint": "например. Андрей, Олег и т. д. ...",
      "winner_screen_titles": "Победитель",
      "winner_screen_chosen_one": "Избранный",
      "winner_screen_congrats": "Поздравляем!",
    },
  };

  String get title {
    return _localizedValues[locale.languageCode]['title'];
  }

  String get choseOneTab {
    return _localizedValues[locale.languageCode]['chose_one_tab'];
  }

  String get composerTab {
    return _localizedValues[locale.languageCode]['composer_tab'];
  }

  String get pickChosenOneBtn {
    return _localizedValues[locale.languageCode]['pick_chosen_one_btn'];
  }

  String get addToListTitle {
    return _localizedValues[locale.languageCode]['add_to_list_title'];
  }

  String get addButton {
    return _localizedValues[locale.languageCode]['add_button'];
  }

  String get cancelButton {
    return _localizedValues[locale.languageCode]['cancel_button'];
  }

  String get addDialogDesc {
    return _localizedValues[locale.languageCode]['add_dialog_desc'];
  }

  String get addDialogTextFieldLabel {
    return _localizedValues[locale.languageCode]['add_dialog_text_field_label'];
  }

  String get addDialogTextFieldHint {
    return _localizedValues[locale.languageCode]['add_dialog_text_field_hint'];
  }

  String get winnerScreenTitles {
    return _localizedValues[locale.languageCode]['winner_screen_titles'];
  }

  String get winnerScreenChosenOne {
    return _localizedValues[locale.languageCode]['winner_screen_chosen_one'];
  }

  String get winnerScreenCongrats {
    return _localizedValues[locale.languageCode]['winner_screen_congrats'];
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of AppLocalizations.
    return new SynchronousFuture<AppLocalizations>(
        new AppLocalizations(locale));
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
